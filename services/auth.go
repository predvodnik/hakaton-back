package services

import (
	"time"

	"crypto/sha256"
	"encoding/base64"

	"github.com/google/uuid"

	"git.radojevic.rs/toolbox/error"

	. "back/redis-connection"
)

func GenerateToken(username string) (string, *error.Error) {
	authUUID, err := uuid.NewRandom()
	if err != nil {
		return "", error.NewInternal("Error generating secure hash", err)
	}

	sessionHash := sha256.New()
	sessionHash.Write([]byte(authUUID.String()))
	sessionHashSum := sessionHash.Sum(nil)

	token := base64.StdEncoding.EncodeToString(sessionHashSum)

	RedisClient.SetEX(RedisContext, "sso-token: "+base64.StdEncoding.EncodeToString(sessionHashSum), username, 5*time.Minute)

	return token, nil
}
