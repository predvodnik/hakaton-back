package config

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/json"
	"encoding/pem"
	"io/ioutil"
)

const (
	defaultFile = "./config.json"
)

var Config config

type config struct {
	File string `short:"c" long:"config"`
	Db   struct {
		Username string `json:"username" long:"db-username"`
		Password string `json:"password" long:"db-password"`
		Host     string `json:"host" long:"db-hostname"`
		Port     string `json:"port" long:"db-port"`
		Name     string `json:"name" long:"db-name"`
	} `json:"database"`
	Redis struct {
		Host     string `json:"host" long:"redis-hostname"`
		Password string `json:"password" long:"redis-password"`
	} `json:"redis"`
	CORS struct {
		Origin string `json:"origin" long:"cors-origin"`
	} `json:"cors"`
	Key struct {
		PrivateFile string `json:"private" short:"k" long:"key"`
		Private     *rsa.PrivateKey
		Public      *rsa.PublicKey
	} `json:"key"`
	Session int    `json:"session" long:"session-length"`
	Host    string `json:"host" short:"h" long:"host"`
	Port    string `json:"port" short:"p" long:"port"`
	Debug   bool   `json:"debug" short:"d" long:"debug"`
	LogFile string `json:"log_file" short:"f" long:"log-file"`
}

func (conf *config) Read() error {
	if len(conf.File) <= 0 {
		conf.File = defaultFile
	}

	data, err := ioutil.ReadFile(conf.File)
	if err != nil {
		return err
	}
	err = json.Unmarshal(data, conf)
	if err != nil {
		return err
	}

	private, err := ioutil.ReadFile(Config.Key.PrivateFile)
	if err != nil {
		return err
	}
	block, _ := pem.Decode(private)
	Config.Key.Private, err = x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return err
	}
	Config.Key.Public = &Config.Key.Private.PublicKey

	return nil
}
