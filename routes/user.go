package routes

import (
	"back/services"
	"net/http"

	"github.com/gin-gonic/gin"

	"back/user"

	. "back/config"
)

func register(context *gin.Context) {
	u := user.User{
		Username: context.PostForm("username"),
		Password: context.PostForm("password"),
	}
	context.Set("user", &u)

	err := u.Check()
	if err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}

	err = u.Register()
	if err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}
}

func login(context *gin.Context) {
	u := user.User{
		Username: context.PostForm("username"),
		Password: context.PostForm("password"),
	}
	context.Set("user", &u)

	if err := u.Login(); err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}
}

func update(context *gin.Context) {
	uInterface, ok := context.Get("user")
	if !ok {
		context.String(http.StatusBadRequest, "No username present")
		context.Abort()
		return
	}

	u := uInterface.(*user.User)
	old, err := user.Get(context.PostForm("username"))
	if err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}

	if u == nil || (u.Username != old.Username && !u.Admin) {
		context.String(http.StatusUnauthorized, "You don't have permission to alter this user")
		context.Abort()
		return
	}

	newUser := user.User{
		Username: context.DefaultPostForm("new_username", old.Username),
		Password: context.DefaultPostForm("new_password", old.Password),
	}

	if u.Username == old.Username {
		context.Set("user", &newUser)
	}

	if err := newUser.Check(); err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}

	if err := old.Update(&newUser); err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}
}

func get(context *gin.Context) {
	u, err := user.Get(context.PostForm("username"))
	if err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}

	context.JSON(http.StatusOK, u)
}

func newSession(context *gin.Context) {
	uInterface, ok := context.Get("user")
	if !ok {
		context.String(http.StatusBadRequest, "No username present")
		context.Abort()
		return
	}
	u := uInterface.(*user.User)

	session, err := services.NewSession(u.Username)
	if err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}
	context.SetCookie("session", session, Config.Session, "/", "", !Config.Debug, true)
}

func loggedIn(context *gin.Context) {
	session, e := context.Cookie("session")
	if e != nil || len(session) == 0 {
		context.String(http.StatusUnauthorized, "Not logged in")
		context.Abort()
		return
	}

	username, err := services.VerifySession(session)
	if err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}

	u, err := user.Get(username)
	if err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}

	context.Set("user", u)
}

func logOut(context *gin.Context) {
	defer context.Next()

	session, e := context.Cookie("session")
	if e != nil {
		return
	}

	context.SetCookie("session", "", 1, "/", "", !Config.Debug, true)

	err := services.RemoveSession(session)
	if err != nil {
		context.String(err.Code, err.Message)
		context.Abort()
		return
	}
}
