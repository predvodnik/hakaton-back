package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"

	. "back/config"
)

func cors(c *gin.Context) {
	c.Writer.Header().Set("Access-Control-Allow-Origin", Config.CORS.Origin)
	c.Writer.Header().Set("Access-Control-Allow-Credentials", "true")
	c.Writer.Header().Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

	if c.Request.Method == "OPTIONS" {
		c.AbortWithStatus(http.StatusNoContent)
		return
	}

	c.Next()
}
